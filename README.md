<a href="https://flathub.org/apps/details/com.gitlab.librebob.Athenaeum">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>



# Athenaeum

A libre replacement for Steam

*In the modern period, the term "Athenaeum" is widely used in various countries for schools, libraries, museums, cultural centers, performance halls and theaters, periodicals, clubs and societies - all aspiring to fulfill a cultural function similar to that of the ancient Roman school.*

Matrix Channel: [#athenaeum:matrix.org](https://matrix.to/#/#athenaeum:matrix.org)

![The main page of Athenaeum.](https://matrix-client.matrix.org/_matrix/media/r0/download/matrix.org/SpPBWUackETZimwnuqsiwBbl/ath_default_screen.png)

## What Works

* Installing Games
* Running Games
* Uninstalling Games
* Updates (Only monolithic atm, individual to come)

## Installing Athenaeum

### Recommended

Get it from Flathub!

https://flathub.org/apps/details/com.gitlab.librebob.Athenaeum

### Arch

<span style="color:red">Out of date and broken. Merge requests to fix the PKGBUILD encouraged.</span>

Download just the `PKGBUILD` and use it to build the package. Once built install it with pacman.

Or get it from the AUR https://aur.archlinux.org/packages/athenaeum-git/

## Development

### Getting Started

Clone this repo.

### Prerequisites

You will need python3 and flatpak.

Setup a python virtual environment, activate it and install python dependencies.

```
python -m venv athenaeum-env
source athenaeum-env/bin/activate
python -m pip install -r requirements.txt
```

### Running Athenaeum

```
python athenaeum.py
```

## Running the tests

```
$ cd test
$ python -m unittest
```

## Getting your game on Athenaeum

Athenaeum uses flatpak as its packaging system and pulls all data from flathub currently.

The best way to get your game on Athenaeum is to create a flatpak config and submit it to the flathub github repository.

https://github.com/flathub/flathub/wiki/App-Submission

Make sure your game appdata.xml contains the `project_license` field with a Free Software license and the `categories` field, with at least the category 'Game'.

If you have the capacity and know how to host a flatpak repository just for libre games, get in touch!

## Built With

* Python3
* PyQt5
* Qt5
* Python-Appstream
* LibreICONS
* Numpy
* Sqlite3

## Contributing

Contributions welcome. No formal process or CLA. Join us on matrix to discuss ideas.

### Translations

#### Using Weblate.org

The kind people of weblate.org have provided Athenaeum with hosting for translations. Using their webapp makes translating a lot easier!

https://hosted.weblate.org/projects/athenaeum/translations/

#### Using QT tools

On Fedora you'll require the qt5-devel package for this.

Athenaeum leverages the QT translation classes and tools. To get started simply add a translation file to `athenaeum.pro` eg. `athenaeum/translations/athenaeum_ja.ts` then run `lupdate -verbose athenaeum.pro` to generate the file.

After that use `linguist` to edit the file adding your translations. To test your translations `lrelease -verbose athenaeum.pro` is used to generate the binary form that is loaded by the app at runtime.

The QT website explains this all better and in more detail.

https://doc-snapshots.qt.io/qt5-5.15/qtlinguist-hellotr-example.html

### Other ways

Find libre games, create flatpak manifests for them and submit them to flathub.

A good place to start finding games is LibreGameWiki.

https://libregamewiki.org/List_of_games

## License

GPLv3 or later (GPLv3+), any other code as licensed (python-appstream is LGPLv2+).

Assets as licensed (LibreICONS is MIT).
