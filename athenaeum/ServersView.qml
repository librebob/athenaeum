import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15
import Qt.labs.qmlmodels 1.0
import Athenaeum 1.0

Page {
    header: NavigationBar {
        activeView: 'servers'
    }
    
    ListView {
        id: serverList
        width: parent.width
        anchors {
            top: tableHeader.bottom
            right: parent.right
            left: parent.left
            bottom: tableControls.top
        }
        boundsBehavior: Flickable.StopAtBounds
        ScrollBar.vertical: ScrollBar { }
        model: serverProvider.servers
        clip: true
        spacing: 2

        delegate: Rectangle {
            color: Material.background
            height: 26
            width: parent ? parent.width : 0
            border.color: ListView.isCurrentItem || serverMouseArea.containsMouse ? Material.accent : Material.background
            border.width: 1

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    serverList.currentIndex = index
                }
                onDoubleClicked: {
                    connectButton.clicked()
                }
                id: serverMouseArea
                hoverEnabled: true
            }

            Row {
                anchors.fill: parent
                spacing: 10
                Image {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 30
                    height: 20
                    fillMode: Image.PreserveAspectFit
                    source: gameId ? serverProvider.availableGames[gameId].iconSmall : null
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 120
                    text: gameId ? serverProvider.availableGames[gameId].name : null
                    elide: Label.ElideRight
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 300
                    text: name || qsTr('?')
                    elide: Label.ElideRight
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 150
                    text: map || qsTr('?')
                    elide: Label.ElideRight
                }
                Label {
                    textFormat: Text.RichText
                    width: 60
                    text: getText()
                    function getText() {
                        if (players == "0") {
                            return players + ' / ' + maxPlayers
                        }
                        return '<b>' + players + '</b> / ' + maxPlayers
                    }
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 250
                    text: address || qsTr('?')
                    elide: Label.ElideRight
                }
                Label {
                    anchors.verticalCenter: parent.verticalCenter
                    width: 50
                    text: country || qsTr('?')
                }
            }
            
        }

        Rectangle {
            z: -10
            anchors.fill: parent
            color: Material.color(Material.Grey, Material.theme == Material.Dark ? Material.Shade900 : Material.Shade300)
        }
    }

    Pane {
        id: tableHeader
        width: parent.width
        padding: 0

        Material.elevation: 2
        Row {
            anchors.fill: parent
            
            spacing: 10
            padding: 10
            
            Label {
                text: "Game"
                width: 150
                font.pixelSize: 18
            }
            Label {
                text: "Name"
                width: 300
                font.pixelSize: 18
            }
            Label {
                text: "Map"
                width: 150
                font.pixelSize: 18
            }
            Label {
                text: "Players"
                width: 60
                font.pixelSize: 18
            }
            Label {
                text: "Address"
                width: 250
                font.pixelSize: 18
            }
            Label {
                text: "Country"
                width: 50
                font.pixelSize: 18
            }
        }
    }

    Pane {
        id: tableControls
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        Material.elevation: 5
        RowLayout {
            anchors.fill: parent
            spacing: 10

            Button {
                text: "Refresh"
                onClicked: {
                    serverProvider.refresh()
                }
            }
            ComboBox {
                Layout.preferredWidth: 200
                textRole: "name"
                valueRole: "id"
                currentIndex: indexOfValue(serverProvider.filterGame)
                model: [{id: null, name: qsTr('All')}].concat(Object.values(serverProvider.availableGames))
                onActivated: {
                    serverProvider.filterGame = currentValue
                }
            }

            CheckBox {
                checked: serverProvider.showEmpty
                text: qsTr("Show empty")
                onClicked: {
                    serverProvider.showEmpty = !serverProvider.showEmpty
                }
            }

            Label {
                enabled: false
                text: qsTr('%L1 servers found.').arg(serverProvider.servers.length)
            }

            Item{
                Layout.fillWidth: true   
            }

            Button {
                id: connectButton
                text: "Connect"
                onClicked: {
                    if (serverList.model[serverList.currentIndex]) {
                        library.joinServer(serverList.model[serverList.currentIndex])
                    }
                }
            }
        }
    }
}
